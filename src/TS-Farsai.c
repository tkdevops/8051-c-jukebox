// " CK05.C51

// FarsaiOke Board
// " 9 �.�. 2546 
// " 	- ����¹��������������繵�ǤǺ��������� ��§���� event ��Ѻ ���������
// " - ¡��ԡ����ʴ���
// " - buadrate = 19200 bps
// " - �ա������ hardlock 
// " - ����¹����觤�ҡ������ credit �ҡ C �� +

// " ����繵���÷��ӧҹ��ǧ�ͺ ����á�˹��� idata ���� data ���
// " global ������Ҩз������ �����繡���红����Ÿ�����

// #define _DEBUG

// " 	part of include LIB file 
#include <reg51.h>

//  part of define
#define _MAX_HARDLOCK_CHAR				10
#define _INSERT_COIN_LEG					P3^2
#define _P3_7_LEG							P3^7	// " ��� setup ��Ҩ�������������С�� credit
#define _DELAY_KEY							52

/* part of setup function prototype */
void setup (void);								// " ����� xx,yy
void sendSerial (unsigned char ch);                                     // " ����� xx,yy


// hardlock function
void sendJunk(void);
void sendAnswer(void);
void hardlockProcess(void);
unsigned char lessThan10(unsigned char xx);

unsigned char readKeyMatrix (unsigned char valueInput);	// " �� xx,yy

/* part of define byte global variable in ram from function */
	idata unsigned char tmpPort;

	idata unsigned char tmpReadkey; 				// " ��ҹ�ҡ����
	idata unsigned char flagKey;							// " Flag �ʴ��ҹ��ҧ� (��)
	
	idata unsigned char m_ucCountHardlock;		// " ��Ѻ����Ѻ��� hardlock �ҡ�����ѡ������
	idata unsigned char m_ucArrBaseHardlock[_MAX_HARDLOCK_CHAR];   // " ˹��¤��������纤�ҷ���Ѻ�Ҩҡ PC
	
// "   0 PROMPT : ����� 1 �ʴ�������Ѻ��õͺ�Ѻ����

/* part of define bit global variable in ram */
	sbit _INSERT_COIN = _INSERT_COIN_LEG;
	sbit _P3_7		=	_P3_7_LEG;
	
	code unsigned char m_codeTable[][10] = {		
												{'B','C','D','F','G','H','J','K','L','M'},
												{'K','L','M','N','P','Q','R','S','T','V'},
												{'Q','R','S','T','V','W','X','Y','Z','B'},
												{'H','J','K','L','M','B','C','D','F','G'},
												{'N','P','Q','R','D','F','G','H','M','N'},
												{'F','G','H','N','P','Q','B','C','D','F'},
												{'T','V','W','X','Y','Z','P','Q','R','S'},
												{'G','H','J','K','L','M','N','P','Q','R'},
												{'K','L','B','C','D','M','N','C','D','F'},
												{'M','N','K','L','B','C','J','K','L','M'},
												{'A','E','I','O','U','A','E','I','O','U'}};            // keyword

void main (void)
{	
	setup();
	while (1)
	{
		//while (!(flagKey & 0x01)) {}				// " ����ҡ����ѧ��������������Ѻ���
		
		// " 1 4 7 *
		tmpReadkey = readKeyMatrix (0x6F); 	// " 0110 1111
		if (tmpReadkey == 0x07)
		{
			// " '*'               			
			sendSerial ('*');
		}
		else if (tmpReadkey == 0x0B)
		{
			// " '7'
			sendSerial ('7');
		}
		else if (tmpReadkey == 0x0D)
		{
			// " '4'
			sendSerial ('4');
		}
		else if (tmpReadkey == 0x0E)
		{
			// " '1'
			sendSerial ('1');
		}

		// " 2 5 8 0
		tmpReadkey = readKeyMatrix (0x5F);  // " 0101 1111
		if (tmpReadkey == 0x07)
		{
			// " '0'               			
			sendSerial ('0');
		}
		else if (tmpReadkey == 0x0B)
		{
			// " '8'
			sendSerial ('8');
		}
		else if (tmpReadkey == 0x0D)
		{
			// " '5'
			sendSerial ('5');
		}
		else if (tmpReadkey == 0x0E)
		{
			// " '2'
			sendSerial ('2');
		}
		
		// " 3 6 9 #
		tmpReadkey = readKeyMatrix (0x3F);  // " 0011 1111
		if (tmpReadkey == 0x07)
		{
			// " '#'
			sendSerial ('#');
		}
		else if (tmpReadkey == 0x0B)
		{
			// " '9'
			sendSerial ('9');
		}
		else if (tmpReadkey == 0x0D)
		{
			// " '6'
			sendSerial ('6');
		}
		else if (tmpReadkey == 0x0E)
		{
			// " '3'
			sendSerial ('3');
		}
	}
}


unsigned char lessThan10(unsigned char xx)
{
	xx &= 0x0F;
	return xx >= 10 ? xx - 10 : xx;
}

void hardlockProcess()
{
	unsigned char ucTmp = 0;
	// xor and save last 4 bit and number must less than 10
	m_ucArrBaseHardlock[0] = lessThan10(0x0F & (m_ucArrBaseHardlock[0] ^= m_ucArrBaseHardlock[4]));
	m_ucArrBaseHardlock[1] = lessThan10(0x0F & (m_ucArrBaseHardlock[1] ^= m_ucArrBaseHardlock[5]));
	m_ucArrBaseHardlock[2] = lessThan10(0x0F & (m_ucArrBaseHardlock[2] ^= m_ucArrBaseHardlock[6]));
	m_ucArrBaseHardlock[3] = lessThan10(0x0F & (m_ucArrBaseHardlock[3] ^= m_ucArrBaseHardlock[7]));
		
	// look up table
	ucTmp = lessThan10(m_ucArrBaseHardlock[8] >> 4);
	m_ucArrBaseHardlock[0] = m_codeTable[ucTmp][m_ucArrBaseHardlock[0]];			// [8] High

	ucTmp = lessThan10(m_ucArrBaseHardlock[8] & 0x0F);
	m_ucArrBaseHardlock[1] = m_codeTable[ucTmp][m_ucArrBaseHardlock[1]];            // [8] Low
	
	ucTmp = lessThan10(m_ucArrBaseHardlock[9] >> 4);
	m_ucArrBaseHardlock[2] = m_codeTable[ucTmp][m_ucArrBaseHardlock[2]];            // [9] High
	
	ucTmp = lessThan10(m_ucArrBaseHardlock[9] & 0x0F);
	m_ucArrBaseHardlock[3] = m_codeTable[ucTmp][m_ucArrBaseHardlock[3]];            // [9] Low
	
	// send answer
	sendJunk();
	sendAnswer();
	sendJunk();
	sendJunk();
}

void sendAnswer()
{  	
	unsigned char yy = lessThan10( (m_ucArrBaseHardlock[0] ^ m_ucArrBaseHardlock[1]) >> 4 ); // random start char
	unsigned char zz = lessThan10( (m_ucArrBaseHardlock[2] ^ m_ucArrBaseHardlock[3]) >> 4 ); // random stop char
	
	sendSerial( m_codeTable[10][yy] );
	sendSerial( m_ucArrBaseHardlock[0] );
	sendSerial( m_ucArrBaseHardlock[1] );
	sendSerial( m_ucArrBaseHardlock[2] );
	sendSerial( m_ucArrBaseHardlock[3] );
	sendSerial( m_codeTable[10][zz] );
	
	//printf ("%c%c%c%c%c",m_codeTable[10][yy],m_ucArrBaseHardlock[0],m_ucArrBaseHardlock[1],m_ucArrBaseHardlock[2],m_ucArrBaseHardlock[3],m_codeTable[10][zz] );
}

void sendJunk()
{
	// send junk no more than 7 (3 bit)
	unsigned char ww = 0; // counter
	unsigned char xx = (m_ucArrBaseHardlock[0] ^ m_ucArrBaseHardlock[1]) >> 5;  // random loop
	unsigned char yy = lessThan10( (m_ucArrBaseHardlock[0] ^ m_ucArrBaseHardlock[1]) >> 4 ); // random column
	unsigned char zz = lessThan10( (m_ucArrBaseHardlock[2] ^ m_ucArrBaseHardlock[3]) >> 4 ); // random row
	for (ww = 0; ww < xx; ww++)
	{
		sendSerial ( m_codeTable[zz][yy] );
	}
}

void sendSerial (unsigned char ch)
{
	if (RI == 0)
	{
		ES = 0;
		SBUF = ch;
		while (TI != 1) {}
		TI = 0;
		ES = 1;
	}
}

void setup (void)
{
	// " ------------- ; hardlock protocal
	flagKey = 0x00;	// " 0000 0000 �Ե 2 ��� start stop protocal
	m_ucCountHardlock = 0x00; // " ���������á
	
	// " ------------- ; port
	P0 = 0xFF;
	P1 = 0xFF;
	P2 = 0xFF;
	P3 = 0xFF;
	
	// " ------------- ; interrupt
	EA = 0x1;		// " �Դ����� INTERRUPT
	ES = 0x1;		// " �Դ INT SERIAL
	PS = 0x1;		// " IP PS = 1

	// " ------------- ; SETUP INTERRUPT EX0
	IT0 = 0x1;		// " TCON REGISTER �ӧҹ���ͺŧ
	EX0 = 0x1;	// " ͹حҵ�����ա���� EX0
	PX0 = 0x1;	// " ����Ҥ����Ӥѭ�٧

	// " ------------- ; SETUP BUADRATE TIMER1
	PCON = PCON | 0x80; // " SMOD = 1
	SCON = 0x50;	// " 01 0 1 0000 �Ǻ��� SERIAL
	TMOD = 0x21; 	// " TIMER1 MODE 2, TIMER0 MODE 1
	TH1 = 0xFD; 	// " �͵�õ 19200 (�ҡ��� 9600)
	TR1 = 0x1;	// " ������͵�õ
}

// " ��ҹ��� keyboard matrix ���觢����Ũе�ͧ�� 4 �Ե��, 4 �Ե��ҧ���Ѻ
unsigned char readKeyMatrix(unsigned char valueInput)
{
	idata unsigned char xx;
	idata unsigned char yy;
	
	P1 = valueInput;		// " �е�ͧŧ���´��� 1111
	tmpPort = P1;		// " read port 1
	tmpPort = tmpPort & 0x0F;	
	if (tmpPort != 0x0F)
	{
		// " delay ˹�ǧ���� Debounce
		for (xx = 0; xx < 147; xx++) 
			for (yy = 0; yy < 120; yy++);
			
		//P1 = valueInput;
		tmpPort = P1;		// " read port 1
		tmpPort = tmpPort & 0x0F;
		if ((tmpPort & 0x0F) != 0x0F) // " �ʴ�����ա�á���ԧ
		{
			for (xx = 0; xx < 147; xx++) 
				for (yy = 0; yy < 120; yy++);
			tmpPort = P1;
			while ((P1 & 0x0F) != 0x0F) {}	// " �ѧ����������
			// " delay ˹�ǧ���� Debounce
						tmpPort = tmpPort & 0x0F;
			return tmpPort;
		}		
		else
		{
			return 255;
		}
	}	
	return 255;	
}

// " external interrupt ����Ѻ�ӹǹ�����ʹ����­
void coin (void) interrupt 0
{
	idata unsigned char xx2;
	idata unsigned char yy2;
	EX0 = 0;
	// " delayCoin () 
	for (xx2 = 0; xx2 < 147; xx2 ++) 
		for (yy2 = 0; yy2 < 80; yy2 ++); // " 30 ms

	if (!_INSERT_COIN)  //// " && (credit < 99))
	{
		if (!_P3_7)   // " 	��Ң� P2.4 set jumper ��� + 1
		{
			////credit++;
			sendSerial('+');
		}
		sendSerial('+');
	}    	
	EX0 = 1;
}

// " serial interrupt ������áѺ computer ���� RS232
void serial (void) interrupt 4
{
	ES = 0;
	if (RI == 1)
	{	
		//if (SBUF == 'P')  // " prompt  ��ͧ����¹��Ẻ�������
		//{			
		//	// " setup flag 0 -> 1
		//	flagKey |= 0x01;		// " 0000 0001			
		//}	
		// " ���� ����ա��������� SBUF ���������͹˹�� protocal ������Ҩ�������Ѻʹ
		if ((SBUF == m_codeTable[10][0]) 
		|| (SBUF == m_codeTable[10][1])
		|| (SBUF == m_codeTable[10][2])
		|| (SBUF == m_codeTable[10][3])
		|| (SBUF == m_codeTable[10][4])
		|| (SBUF == m_codeTable[10][5])
		|| (SBUF == m_codeTable[10][6])
		|| (SBUF == m_codeTable[10][7])
		|| (SBUF == m_codeTable[10][8])
		|| (SBUF == m_codeTable[10][9]))
		{
			flagKey |= 0x02;
		}
		else
		{
			// " ����繤������ �������� AEIOU ��� flagKey �ѧ��ʶҹ��Ѻ�������ѹ�֡
			if ((flagKey & 0x02) == 0x02)
			{			
				m_ucArrBaseHardlock[m_ucCountHardlock] = SBUF;
				m_ucCountHardlock++;
				if (m_ucCountHardlock >= _MAX_HARDLOCK_CHAR)
				{
					m_ucCountHardlock = 0x0; // reset

					RI = 0; // ***
					hardlockProcess();  // " �������û����ż� Hardlock
					flagKey &= 0xFD; // " ¡��ԡ flagKey
				}
			}
		}		
		RI = 0;
	}
	ES = 1;
}
