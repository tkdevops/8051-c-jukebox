/*
	Complete !!!

	project	: Cholatee Karaoke Board
	start		: 12 December 2003 16:25
	end		: 27 March 2004 23:42
	target	: AT89C52 clock 11.0592Mhz
	by			: BenNueng 
*/

/* 18/3/2547
	+: hardlock
	+: clean
	+: �ء���ҧ�Ǻ����ªŸ�
*/


/*
PC Protocal

set display number 
\n_Cxxx\n	: \n : start - stop protocal
			: _ : start cmd
			: C : cmd
			: xxx : value

MCU -> PC
%Txxx%   : ����
%Cxxx%	: credit
*/

/*	part of include LIB file */
#include <reg52.h>
#include <stdio.h> 


/* part of define */
//#define __DEBUG__MODE__					1
//#define __RELEASE__MODE__				1

#define _MAX_LOCATION_NUMBER			10			// �ӹǹ���˹� 7segment ������
#define _MAX_ARRAY_NUMBER				5			// ��Ҵ���������红����� _MAX_LOCATION_NUMBER (1 byte = 2 �ѡ��)
#define _MAX_SONG								20			// �ŧ�٧�ش�������_ARRAY_BUFFER / 3
#define _INSERT_COIN_LEG					P3^2		// ����ʹ����­ 1
#define _INSERT_COIN_LEG2					P3^3		// ����ʹ����­ 2
#define _P2_4_LEG									P2^4		// DipSwitch 1
#define _P2_5_LEG									P2^5		// DipSwitch 2
#define _P2_6_LEG									P2^6		// DipSwitch 3
#define _P2_7_LEG									P2^7		// DipSwitch 4
#define _P3_SOUND1_LEG						P3^4		// Sound1
#define _P3_SOUND2_LEG						P3^5		// Sound2
#define _P3_I2C_DATA_LEG						P3^6		// I2C data leg  WR 16 (�� 2)
#define _P3_I2C_CLOCK_LEG					P3^7		// I2C clock leg  RD 17 (���á)
#define _MAX_HARDLOCK_CHAR				10			// �ӹǹ����ѡ�� �٧�ش �ͧ hardlock
#define _MAX_CMD_CHAR						10			// �ӹǹ������٧�ش �ҡ PC

#define __100ms__						92100
#define __30ms__							27649
#define __25ms__							23041
#define __20ms__							18420
#define __10ms__							9210
#define __1ms__							921

#define true								1
#define false								0
#define byte								unsigned char
#define word								unsigned int


/* part of setup function prototype */
void setup (void);										
void sendSerial (byte ch);								
void setNumberDisplay (byte location, byte number);		
void delayCycle(word cycle);
void soundNext();
void processCmd();
void clearCmd();
byte readKeyMatrix (byte valueInput);					
byte hex2byte (char ch);

/* part of define byte global variable */
byte displayNumber[_MAX_ARRAY_NUMBER];			// 1 byte = 2 char
byte m_bArrCmd[_MAX_CMD_CHAR];	// �纤���觨ҡ PC
byte m_ucArrBaseHardlock[_MAX_HARDLOCK_CHAR];   // " ˹��¤��������纤�ҷ���Ѻ�Ҩҡ PC

byte displayLocation;
byte m_bSerialCount;							// ��Ѻ����Ѻ��� char �ҡ serial �ҡ�����ѡ������
byte tmpReadkey; 									// ��ҹ�ҡ����
byte flagKey;											// Flag ��������Ѻ�ҹ��ҧ�
//byte credit;   											// �纨ӹǹ credit  -> (^^) ��������� 27/8/2547
byte m_bProtocalStatus;						// �����-������Ѻ��Ҩҡ PC
byte m_countBaseHardlock;					// �Ѻ��� hardlock

//;		0 PROMPT : ����� 1 �ʴ�������Ѻ��õͺ�Ѻ����
//;		1 COM PLAY  : ����� 1 �ʴ���Ҥ�����������ѧ����ŧ����
//;		2 TIME DISPLAY : ����� 1 �ʴ���ҡ��ѧ���Ţ��衴���������͡�����ź
//;		3 PUSH CONTINUE : ����� 1 �ʴ�����ѧ���ŧ�������
//;		4 SPK0 : �Ե���;  00 = nosound; 01 = left; 11 = both; 10; right
//;		5 SPK1 : �Ե�٧;

// /////// part of define bit global variable in ram //////////////////////////////////////////// 
sbit _INSERT_COIN	= _INSERT_COIN_LEG;						// ����ʹ����­��� 1
sbit _INSERT_COIN2	= _INSERT_COIN_LEG2;					// ����ʹ����­��� 2

sbit _P2_4			= _P2_4_LEG;							// dipswitch 1
sbit _P2_5			= _P2_5_LEG;							// dipswitch 2
sbit _P2_6			= _P2_6_LEG;							// dipswitch 3
sbit _P2_7			= _P2_7_LEG;							// dipswitch 4

sbit _P3_Sound1		= _P3_SOUND1_LEG;
sbit _P3_Sound2		= _P3_SOUND2_LEG;

// /////// i2c define port  ////////////////////////////////////////////////////////////////// 
sbit SDA = _P3_I2C_DATA_LEG;						// I2C data leg  WR 16 (�� 2)
sbit SCL = _P3_I2C_CLOCK_LEG;					// I2C clock leg  RD 17 (���á)

// /////// 7 Segment  ////////////////////////////////////////////////////////////////// 
code byte numberTable[] = 
{
	0x3F	 ,0x06	,0x5B	,0x4F	,0x66	,0x6D	,0x7D	,0x07	,
//	0		1			2			3			4			5			6			7
	0x7F	,0x67	,0x79	,0x50	,0x38	,0xFF	,0x40	,0x00	,
//	8    	9			E			r			L			8			.			-     
	0x76	,0x71	,0x3E	,0x5C	,0x39
//	H       F			U			o			C
};


// /////// hardlock ////////////////////////////////////////////////////////////////// 
// hardlock function
void hardlockProcess(void);
byte lessThan10(byte xx);

// code
code byte m_codeTable[][10] = {		
												{'B','C','D','F','G','H','J','K','L','M'},
												{'K','L','M','N','P','Q','R','S','T','V'},
												{'Q','R','S','T','V','W','X','Y','Z','B'},
												{'H','J','K','L','M','B','C','D','F','G'},
												{'N','P','Q','R','D','F','G','H','M','N'},
												{'F','G','H','N','P','Q','B','C','D','F'},
												{'T','V','W','X','Y','Z','P','Q','R','S'},
												{'G','H','J','K','L','M','N','P','Q','R'},
												{'K','L','B','C','D','M','N','C','D','F'},
												{'M','N','K','L','B','C','J','K','L','M'},
												{'A','E','I','O','U','A','E','I','O','U'}};            // keyword

code byte m_key[][4] = {
									{'#','0','*','A'},
									{'9','8','7','B'},
									{'6','5','4','C'},
									{'3','2','1','D'}
									};

code byte m_keyValue[] = {0x07,0x0B,0x0D,0x0E};
code byte m_keyCheck[] = {0xE,0xD,0xB,0x7};
										

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
byte x,y;
byte cTmpChKey;
byte cTmpReturnKey;

void main (void)
{
	setup(); 

	while (!(flagKey & 0x01)) {}			// ����ҡ����ѧ��������������Ѻ���
	
	// �Ѻ��� key
	// ��ѡ��� : ����͹�Ե����� + 0x0F ���ǵ�Ǩ
	while( true)
	{
		cTmpChKey = 0xF7; //1111 0111
		for (x = 0 ; x < 4 ; x++ )
		{
			cTmpChKey = (cTmpChKey << 1) + 1;  // ex: 1110 1111 .. .. .
			cTmpReturnKey = readKeyMatrix(cTmpChKey);

			for (y = 0; y < 4 ; y++)
			{
				if (cTmpReturnKey == m_keyCheck[y])
				{
					sendSerial(m_key[y][x]);
				}
			} // end for y
		} // end for x
	} // while loop true
}

void setup (void)
{
	byte bCount = 0;

	// ------------- ; port
	P0 = 0xFF;
	P1 = 0xFF;
	P2 = 0xFF;
	P3 = 0xFF;	// set relay TV off

	//_P3_Sound1 = 0;
	//_P3_Sound2 = 0;
	SCL = 0; // �Դ sound  ��͹ ����ͷӧҹ�֧�Դ 27/7/2547

	// ------------- ; interrupt
	EA = 0x1;			// �Դ����� INTERRUPT
	ES = 0x1;			// �Դ INT SERIAL
	PS = 0x1;			// IP PS = 1

	// ------------- ; SETUP INTERRUPT EX1
	IT1 = 0x1;			// TCON REGISTER �ӧҹ���ͺŧ
	EX1 = 0x1;		// ͹حҵ�����ա���� EX1
	PX1 = 0x1;		// ����Ҥ����Ӥѭ�٧

	// ------------- ; SETUP INTERRUPT EX0
	IT0 = 0x1;			// TCON REGISTER �ӧҹ���ͺŧ
	EX0 = 0x1;		// ͹حҵ�����ա���� EX0
	PX0 = 0x1;		// ����Ҥ����Ӥѭ�٧

	// ------------- ; SETUP BUADRATE TIMER1
	// PCON = PCON | 0x80; // �Ե SMOD = 1 ����� PCON ���������Ѵ��ѧ�ҹ
	SCON = 0x50;		// 01 0 1 0000 �Ǻ��� SERIAL
	TMOD = 0x21; 		// TIMER1 MODE 2, TIMER0 MODE 1
	TH1 = 0xFD; 			// �͵�õ 9600 (19200 ���ͧ�絷�� SMOD = 1)
	TR1 = 0x1;			// ������͵�õ

	// ------------- ; SETUP TIMER0 ����ҳ 2ms
	ET0 = 0x1;			// �Դ INT TIMER0
	TH0 = 0xF8; 			// ��˹������� TIMER0 (�ҡ��� 0xF8)
	TL0 = 0xCC;			// �ҡ��� 0xCC
	TR0 = 0x1;			// �������ҹ TIMER0
	
	// ------------- ; SETUP General program
	displayLocation		= 0;
	m_bSerialCount	= 0;					// ��Ѻ����Ѻ��� char �ҡ serial �ҡ�����ѡ������
	tmpReadkey			= 0; 					// ��ҹ�ҡ����
	flagKey					= 0;					// Flag ��������Ѻ�ҹ��ҧ�
	//credit					= 0;   				// �纨ӹǹ credit
	m_bProtocalStatus		= 0;			// �����-������Ѻ��Ҩҡ PC
	m_countBaseHardlock	= 0;			// �Ѻ��� hardlock


	// ����Ţ�ʴ��� ��ѡ�á����ҧ��� ������ҡ 1
	setNumberDisplay (10,14);
	setNumberDisplay (9,14);
	setNumberDisplay (8,14);
	setNumberDisplay (7,14);
	setNumberDisplay (6,14);
	setNumberDisplay (5,14);
	setNumberDisplay (4,0);
	setNumberDisplay (3,0);
	setNumberDisplay (2,0);
	setNumberDisplay (1,0);
	

	// ˹�ǧ���ҡ�͹�Դ
	//for (bCount = 0; bCount < 200 ;  bCount++)
	//{
	//	delayCycle(__30ms__);
	//}

	//_P3_Sound1 = 1;// �Դ tv
	//_P3_Sound2 = 1;// �Դ tv
}

// ��ҹ��� keyboard matrix ���觢����Ũе�ͧ�� 4 �Ե��, 4 �Ե��ҧ���Ѻ
byte readKeyMatrix(byte valueInput)
{
	byte tmpPort;
	
	P1 = valueInput;		// �е�ͧŧ���´��� 1111
	tmpPort = P1;			// read port 1
	tmpPort = tmpPort & 0x0F;	
	if (tmpPort != 0x0F)
	{
		// delay ˹�ǧ���� Debounce
		delayCycle(__25ms__);
			
		//P1 = valueInput;
		tmpPort = P1;		// read port 1
		tmpPort = tmpPort & 0x0F;
		if ((tmpPort & 0x0F) != 0x0F) // �ʴ�����ա�á���ԧ
		{
			// debounce
			delayCycle(__25ms__);

			tmpPort = P1 & 0x0F;
			while ((P1 & 0x0F) != 0x0F) {}	// �ѧ����������
			
			// delay ˹�ǧ���� Debounce
			return tmpPort;
		}		
		else
		{
			return 255;
		}
	}	
	return 255;	
}

void sendSerial (byte ch)
{
	//ES = 0;
	//if (RI==0)
	//{
		SBUF = ch;
		while (TI != 1) {}
		TI = 0;
	//}
	//ES = 1;
}


// ��������˹� location �դ�� number �����ʴ��� �� location �������� 1 ����
void setNumberDisplay (byte location, byte number)
{
	byte tmpModNumber2;
	byte tmpDivNumber2;

	// ��� mod ��������� ����ͧź 1
	// ��� mod ������������ ���ź 1
	tmpModNumber2 = location % 2;
	tmpDivNumber2 = location / 2;
	
	if (tmpModNumber2 == 0)
	{
		tmpDivNumber2--;		
	}
	
	// �������������ҵ����ҧ ����������ҵ�Ǻ�
	if (tmpModNumber2 > 0)
	{
		displayNumber[tmpDivNumber2] = (displayNumber [tmpDivNumber2] & 0xF0) + (number & 0x0F);
	}
	else
	{
		displayNumber[tmpDivNumber2] = (displayNumber [tmpDivNumber2] & 0x0F) + (number << 4);
	}
}

// timer 0 interrupt ����Ѻ�ʴ��ŵ���Ţ
void timer0 (void) interrupt 1
{
	byte tmpDivNumber;
	byte tmpModNumber;

	TR0 = 0;// ��ش�����ҹ Timer 0
	if (++displayLocation > _MAX_LOCATION_NUMBER)
	{
		displayLocation = 1;
	}

	// �¡��Ҥ�ҵ���Ţ��Ш���ѡ����� port 0
	// ��� mod ��������� ����ͧź 1
	// ��� mod ������������ ���ź 1
	tmpModNumber = displayLocation % 2;
	tmpDivNumber = displayLocation / 2;
	
	if (tmpModNumber == 0)
	{
		tmpDivNumber--;		
	}
	
	// �������������ҵ����ҧ ����������ҵ�Ǻ�
	if (tmpModNumber > 0)
	{
		P0 = numberTable[displayNumber [tmpDivNumber] & 0x0F];	
	}
	else
	{
		P0 = numberTable[displayNumber [tmpDivNumber] >> 4];	
	}

	// ��˹��ʴ��Ť�һѨ�غѹ � ��ѡ��ҧ�
	P2 = 0xF0;
	
	P2 += (displayLocation-1); // 74LS145 start at 0  
	//P2 += 10 - displayLocation; // *****��䢡���ʴ��Ũҡ����仢�ҷ��ç���
		
	// ��ǹ��͹��	
	TH0 = 0xF8; 		// ��˹������� TIMER0 (�ҡ��� 0xF8)
	TL0 = 0xCC;			// �ҡ��� 0xCC
	TF0 = 0;			// clear FLAG
	TR0 = 1;			// ����������ҹ Timer 0
}

// external interrupt ����Ѻ�ӹǹ�����ʹ����­
void coin (void) interrupt 0
{
	idata unsigned char xx2;
	idata unsigned char yy2;
	byte bTmp = 0;
	
	EX0 = 0;
	
	// delayCoin () ; debounce
	// 8 march 2547
	// delayCycle(__30ms__);

	// delayCoin () 
	for (xx2 = 0; xx2 < 147; xx2 ++) 
		for (yy2 = 0; yy2 < 80; yy2 ++); // 30 ms

	// read port		
	P2 |= 0xF0; // set port 2 is 1111 0000
	bTmp = (P2 & 0x20) >> 5;      // 0001 0000 read bit 4 of port 2
		
	if (bTmp)   //	��Ң� P2.5 ��� set jumper ��� + 1
	{
		sendSerial('Y');

		//-> (^^) ��������� 27/8/2547
		//if ( !(flagKey & 0x01) )
		//{
		//	credit++;
		//}
	}
	else
	{
		sendSerial('Y');
		sendSerial('Y');

		//-> (^^) ��������� 27/8/2547
		//if ( !(flagKey & 0x01) )
		//{
		//	credit += 2;
		//}
	}


	//if ( !(flagKey & 0x01) )
	//{
	//	setNumberDisplay (2,credit / 10);
	//	setNumberDisplay (1,credit % 10);
	//}

	EX0 = 1;
}

// external interrupt ����Ѻ�ӹǹ�����ʹ����­ 2
void coin2 (void) interrupt 2
{
	idata unsigned char xx2;
	idata unsigned char yy2;
	byte bTmp = 0;
	
	EX1 = 0;
	
	// delayCoin () ; debounce	
	// 8 march 2547
	// delayCycle(__30ms__);
	for (xx2 = 0; xx2 < 147; xx2 ++) 
		for (yy2 = 0; yy2 < 80; yy2 ++); // 30 ms
	
	// read port		
	P2 |= 0xF0;							// set port 2 is 1111 0000
	bTmp = (P2 & 0x20) >> 5;     // 0001 0000 read bit 4 of port 2
		
	if (bTmp)	//	��Ң� P2.5 ��� set jumper ��� + 1
	{
		sendSerial('Y');
		sendSerial('Y');
		
		//-> (^^) ��������� 27/8/2547
		// ����ѧ����������ǡ���
		//if ( !(flagKey & 0x01) )
		//{
		//	credit += 2;
		//}
	}
	else
	{
		sendSerial('Y');
		sendSerial('Y');
		sendSerial('Y');
		sendSerial('Y');

		//-> (^^) ��������� 27/8/2547
		// ����ѧ����������ǡ���
		//if( !(flagKey & 0x01) )
		//{
		//	credit += 4;
		//}
	}

	//-> (^^) ��������� 27/8/2547
	// ����ѧ�����������ʴ���
	//if( !(flagKey & 0x01) )
	//{
	//	setNumberDisplay (2,credit / 10);
	//	setNumberDisplay (1,credit % 10);
	//}

	EX1 = 1;
}

// serial interrupt ������áѺ computer ���� RS232
void serial (void) interrupt 4
{
	/* idata *//* byte count;*/
	/* idata *//*byte location;*/
	
	//ES = 0;
	if (RI == 1)
	{
		if (SBUF == 13 || SBUF == '\n') // start protocal
		{
			if (m_bProtocalStatus == 1)
			{
				m_bProtocalStatus = 0;
				processCmd();
			}
			else
			{
				m_bProtocalStatus = 1;
			}
		}
		else
		{
			if (m_bProtocalStatus == 1 && m_bSerialCount < _MAX_CMD_CHAR)
			{
				m_bArrCmd[m_bSerialCount] = SBUF;
				m_bSerialCount++;
			}
			else
			{
				// ����բ������������������Դ '\n' ��� save ������ҧ��������
				m_ucArrBaseHardlock[m_countBaseHardlock] = SBUF;
				if (m_countBaseHardlock < _MAX_HARDLOCK_CHAR)
				{
					m_countBaseHardlock++;
				}
				else
				{
					m_countBaseHardlock = 0;
				}
			}
		}
		RI = 0;
	}
	//ES = 1;
}

// delay by Timer2; 
// ����˹����ҷ���ͧ����� cycle ����ͧ���˹�ǧ
void delayCycle(word cycle)
{	
	TF2 = 0;
	TR2 = 0;
	T2CON = 0x00;

	TH2 = cycle >> 8;	
	TL2 = cycle & 0x00FF;
	TR2 = 1;
	while(!TF2){}
	TF2 = 0;
	TR2 = 0;
}


void soundNext()
{
	_P3_Sound1 = !_P3_Sound1;
	_P3_Sound2 = !_P3_Sound2;
}

byte hex2byte (char ch)
{
	ch = ch - 48;
	if (ch > 9)
	{
		ch = ch - 7;
	}
	return ch;
}

byte lessThan10(byte xx)
{
	xx &= 0x0F;
	return xx >= 10 ? xx - 10 : xx;
}

void hardlockProcess()
{
	/*
		��ѡ��� : PC ���� code table ����͹�ѹ

		���� m_bArrCmd
		[2] = ᡹ x1
		[3] = ᡹ y1
		[4] = ����觤ӵͺ������� byte ���...2..6 ��ҹ��
		[5] = ᡹ x2
		[6] = ᡹ y2
		[7] = ᡹ x3
		[8] = ᡹ y3
		[9] = �����

		- x,y �������Թ 9 
		
		����觤ӵͺ��Ѻ 10 byte
		[A,E,I,O,U]..�ӵͺ ����� byte ����к�...[A,E,I,O,U]

		
	*/

	unsigned char btmp = 0;
	unsigned char xx = 0;
	unsigned char yy = 0;

	m_ucArrBaseHardlock[0] = '%';
	m_ucArrBaseHardlock[1] = 'T';
	m_ucArrBaseHardlock[9] = '%';

	// send answer
	btmp = hex2byte(m_bArrCmd[4]); // start answer
	xx = hex2byte(m_bArrCmd[2]);
	yy = hex2byte(m_bArrCmd[3]);
	m_ucArrBaseHardlock[btmp] = m_codeTable[ xx ] [ yy ];

	btmp++;
	xx = hex2byte(m_bArrCmd[5]);
	yy = hex2byte(m_bArrCmd[6]);
	m_ucArrBaseHardlock[btmp] = m_codeTable[ xx ] [ yy ];

	btmp++;
	xx = hex2byte(m_bArrCmd[7]);
	yy = hex2byte(m_bArrCmd[8]);
	m_ucArrBaseHardlock[btmp] = m_codeTable[ xx ] [ yy ];

	// �觤ӵͺ
	for ( btmp = 0; btmp < _MAX_HARDLOCK_CHAR ; btmp++ )
	{
		sendSerial( m_ucArrBaseHardlock[btmp] );
	}
}

void clearCmd()
{
	byte x;
	for (x = 0; x < _MAX_CMD_CHAR; x++)
	{
		m_bArrCmd[x] = ' '; //���繤����ҧ
	}

	m_bSerialCount = 0; // �������ùѺ����
}

void processCmd()
{
	byte x = 0;
	byte bTmp = 0;
	byte bTmp2 = 0;

	// '_P' // prompt
	if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'P')
	{
		// setup flag 0 -> 1
		flagKey |= 0x01;		// 0000 0001
				
		// setup number
		setNumberDisplay (10,15);
		setNumberDisplay (9,15);
		setNumberDisplay (8,15);
		setNumberDisplay (7,15);
		setNumberDisplay (6,15);
		setNumberDisplay (5,15);

		// open port for connect sound and picture			
		_P3_Sound1 = 1;
		_P3_Sound2 = 1;
	}

	// '_S' // set number display
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'S')
	{
		setNumberDisplay ( hex2byte(m_bArrCmd[2]), hex2byte(m_bArrCmd[3]));
	}

	// '_M // Sound next
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'M')
	{
		if (m_bArrCmd[2] == '0')
		{
			_P3_Sound1 = 0;
			_P3_Sound2 = 0;
		}
		else
		{
			_P3_Sound1 = 1;
			_P3_Sound2 = 1;
		}
	}

	// '_N' // Sound next
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'N')
	{
		soundNext();
	}

	// '_R // i2c Read leg
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'R')
	{
		if (m_bArrCmd[2] == '0')
		{
			SCL = 0;
		}
		else
		{
			SCL = 1;
		}
	}


	// '_W // i2c Write leg
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'W')
	{
		if (m_bArrCmd[2] == '0')
		{
			SDA = 0;
		}
		else
		{
			SDA = 1;
		}
	}

	// '_X' // hardlock process
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'X')
	{
		hardlockProcess();
	}

		// ������� bonus ����� 15/4/2548
		// '_B' // Bonus process
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'B')
	{
		// �������÷ӧҹ����ǡѺ bonus �ç���
		// BonusFunction();
	}

	//-> (^^) ��������� 27/8/2547
	// '_G' // get credit
	//else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'G')
	//{
	//	sendSerial('%');
	//	sendSerial('C');
	//	sendSerial(credit);
	//	sendSerial('%');
	//}

	//// '_C' // set credit
	//else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'C')
	//{
	//	credit = m_bArrCmd[2];
	//	setNumberDisplay (2,credit / 10);
	//	setNumberDisplay (1,credit % 10);
	//}

	// '_D' // chk Dip Switch
	else if (m_bArrCmd[0] == '_' && m_bArrCmd[1] == 'D')
	{
		// read port		
		P2 |= 0xF0;							// set port 2 is 1111 0000
		bTmp = (P2 & 0xF0) >> 3;     // 0001 0000 read bit 4 of port 2
		
		sendSerial ('%');
		sendSerial ('D');

		// start bit 4567
		for (x = 0; x < 4 ; x++ )
		{

			bTmp = bTmp >> 1;
			bTmp2 = bTmp;
			bTmp2 = bTmp2 & 0x01;
			if (bTmp2 == 1)
			{
				sendSerial('1');
			}
			else
			{
				sendSerial ('0');
			}
		}

		sendSerial ('%');
	}
	
	clearCmd();
}
