/*	part of include LIB file */
#include <reg51.h>

/* part of define */
#define _MAX_LOCATION_NUMBER				10
#define _MAX_ARRAY_NUMBER					5
#define _ARRAY_BUFFER						60
#define _MAX_SONG							20			// _ARRAY_BUFFER div 3
#define _INSERT_COIN_LEG					P3^2
#define _P2_4_LEG							P2^4
#define _DELAY_KEY							52
#define _CURRENT_SONG						3

/* part of setup function prototype */
void setup (void);								// ����� xx,yy
void sendSerial (unsigned char ch);                                     // ����� xx,yy
void keyControl (unsigned char value);					// ����� xx,yy
void setNumberDisplay (unsigned char location, unsigned char number);	// ����� xx,yy
unsigned char readKeyMatrix (unsigned char valueInput);	// �� xx,yy

/* part of define byte global variable in ram from function */
// void timer0 (void) interrupt 1
	idata unsigned char tmpDivNumber;
	idata unsigned char tmpModNumber;
	idata unsigned char displayLocation;

//	void keyControl (unsigned char value)
	idata unsigned char countKey;

// unsigned char readKeyMatrix(unsigned char valueInput)
	idata unsigned char tmpPort;

// void setNumberDisplay (unsigned char location, unsigned char number)
	idata unsigned char	tmpModNumber2;
	idata unsigned char	tmpDivNumber2;
	
/* part of define byte global variable */
data unsigned char songMemory[_ARRAY_BUFFER];	// save song 3 byte per 1 song (30 credit)
data unsigned char currentSong[_CURRENT_SONG];	// �ѹ�֡������������Ѻ�ʴ���

idata unsigned char credit;   					// �纨ӹǹ credit
idata unsigned char allSong;					// ��������ŧ����ͷ���������ŧ
idata unsigned char displayNumber[_MAX_ARRAY_NUMBER];		// 1 byte = 2 char
idata unsigned char tmpReadkey; 				// ��ҹ�ҡ����
idata unsigned char flagKey;					// Flag �ʴ��ҹ��ҧ� (��)
idata unsigned char delayTimeDisplay;				// ˹�ǧ���ҵ͹���ŧ����
idata unsigned char delayTimeDisplay2;			// ˹�ǹ���ҵ͹���ŧ����

//;   0 PROMPT : ����� 1 �ʴ�������Ѻ��õͺ�Ѻ����
//;   1 COM PLAY  : ����� 1 �ʴ���Ҥ�����������ѧ����ŧ����
//;   2 TIME DISPLAY : ����� 1 �ʴ���ҡ��ѧ���Ţ��衴���������͡�����ź
//;   3 PUSH CONTINUE : ����� 1 �ʴ�����ѧ���ŧ�������

/* part of define bit global variable in ram */
sbit _INSERT_COIN = _INSERT_COIN_LEG;
sbit _P2_4		=	_P2_4_LEG;

/* part of define global variable in rom*/
code unsigned char numberTable[] = {
					0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,
//			0      1      2       3       4      5      6       7
					0x7F,0x67,0x71,0x3E,0x38,0x77,0x40,0x00,
//			8    	9     F      U      L      A       -     
					0x76,0x79,0x50,0x5C,0x39};
//			H       E      r      o       C

void main (void)
{
	setup();
	while (1)
	{
		while (!(flagKey & 0x01)) {}				// ����ҡ����ѧ��������������Ѻ���
		
		// 1 4 7 *
		tmpReadkey = readKeyMatrix (0x6F); 	// 0110 1111
		if (tmpReadkey == 0x07)
		{
			// '*'  ����¹���§���¢��
			sendSerial('*');
		}
		else if (tmpReadkey == 0x0B)
		{
			// '7'
			keyControl (7);
		}
		else if (tmpReadkey == 0x0D)
		{
			// '4'
			keyControl (4);
		}
		else if (tmpReadkey == 0x0E)
		{
			// '1'
			keyControl (1);
		}

		// 2 5 8 0
		tmpReadkey = readKeyMatrix (0x5F);  // 0101 1111
		if (tmpReadkey == 0x07)
		{
			// '0'               			
			keyControl (0);
		}
		else if (tmpReadkey == 0x0B)
		{
			// '8'
			keyControl (8);
		}
		else if (tmpReadkey == 0x0D)
		{
			// '5'
			keyControl (5);
		}
		else if (tmpReadkey == 0x0E)
		{
			// '2'
			keyControl (2);
		}
		
		// 3 6 9 #
		tmpReadkey = readKeyMatrix (0x3F);  // 0011 1111
		if (tmpReadkey == 0x07)
		{
			// '#'
			sendSerial('#');
			keyControl (15);
		}
		else if (tmpReadkey == 0x0B)
		{
			// '9'
			keyControl (9);
		}
		else if (tmpReadkey == 0x0D)
		{
			// '6'
			keyControl (6);
		}
		else if (tmpReadkey == 0x0E)
		{
			// '3'
			keyControl (3);
		}
	}
}

// �Ѻ��ҡ�á������ҨѴ�����ФǺ�������ʴ���
void keyControl (unsigned char value)
{
	if (credit <= 0) return; 				// ��� credit �� 0
	if (allSong >= _MAX_SONG) return; // ����ŧ���

	// �觢�����仺͡����������
	if (value < 15)
	{
		sendSerial(value+48);
	}

	// ��Ҥ���� 15 ��͡�����������
	if (value == 15)
	{
		countKey = 0;
		setNumberDisplay (5,(currentSong[0] & 0x0F));
		setNumberDisplay (6,(currentSong[0] >> 4));
		setNumberDisplay (7,(currentSong[1] & 0x0F));
		setNumberDisplay (8,(currentSong[1] >> 4));
		setNumberDisplay (9,(currentSong[2] & 0x0F));
		setNumberDisplay (10,(currentSong[2] >> 4));
	}
	else // if (credit > 0)
	{
		countKey++;
		if (countKey == 1)
		{
			// ¡��ԡ�������������ź�Ţ (clr bit 2) ��� set ʶҹ�������������ҧ��á����� (set bit 3)
			flagKey &= 0xFB;		// 1111 1011
			flagKey |= 0x08;		// 0000 1000
			setNumberDisplay (10,value);
			setNumberDisplay (9,15);
			setNumberDisplay (8,15);
			setNumberDisplay (7,15);
			setNumberDisplay (6,15);
			setNumberDisplay (5,15);
		}
		else if (countKey == 2)
		{
			setNumberDisplay (9,value);
		}
		else if (countKey == 3)
		{
			setNumberDisplay (8,value);
		}
		else if (countKey == 4)
		{
			setNumberDisplay (7,value);
		}
		else if (countKey == 5)
		{
			setNumberDisplay (6,value);
		}
		else if (countKey == 6)
		{
			setNumberDisplay (5,value);
			countKey = 0;					// ������Ѻ�������
			delayTimeDisplay = 4;			// ��˹����˹�ǧ���� 4 �ͺ �ͺ�� 500 MS
			flagKey	&= 0xF7;				// �͡��ҡ������������� 1111 0111
			flagKey |= 0x04;				// �͡������Ţ�͡�����ź 0000 0100
			
			// ����ŧ���� songMemory
			songMemory[allSong*3] = displayNumber[4];
			songMemory[allSong*3+1] = displayNumber[3];
			songMemory[allSong*3+2] = displayNumber[2];
			
			// �����ŧ���Ŵ credit
			if (++allSong == _MAX_SONG)
			{
				// �ʴ������ Full
				setNumberDisplay (10,14);
				setNumberDisplay (9,10);
				setNumberDisplay (8,11);
				setNumberDisplay (7,12);
				setNumberDisplay (6,12);
				setNumberDisplay (5,14);
			}
			
			// Ŵ credit
			credit--;			
			
			// updateCredit & allSong
			setNumberDisplay (4,allSong / 10);	
			setNumberDisplay (3,allSong % 10);
			setNumberDisplay (2,credit / 10);
			setNumberDisplay (1,credit % 10);
		}
	}
}

void sendSerial (unsigned char ch)
{
	ES = 0;
	SBUF = ch;
	while (TI != 1) {}
	TI = 0;
	ES = 1;
}

void setup (void)
{
	// ------------- ; port
	P0 = 0xFF;
	P1 = 0xFF;
	P2 = 0xFF;
	P3 = 0xFF;
	
	// ------------- ; interrupt
	EA = 0x1;		// �Դ����� INTERRUPT
	ES = 0x1;		// �Դ INT SERIAL
	PS = 0x1;		// IP PS = 1

	// ------------- ; SETUP INTERRUPT EX0
	IT0 = 0x1;		// TCON REGISTER �ӧҹ���ͺŧ
	EX0 = 0x1;	// ͹حҵ�����ա���� EX0
	PX0 = 0x1;	// ����Ҥ����Ӥѭ�٧

	// ------------- ; SETUP BUADRATE TIMER1
	SCON = 0x50;	// 01 0 1 0000 �Ǻ��� SERIAL
	TMOD = 0x21; 	// TIMER1 MODE 2, TIMER0 MODE 1
	TH1 = 0xFD; 	// �͵�õ 9600
	TR1 = 0x1;	// ������͵�õ

	// ------------- ; SETUP TIMER0
	ET0 = 0x1;	// �Դ INT TIMER0
	TH0 = 0xF8; 	// ��˹������� TIMER0 (�ҡ��� 0xF8)
	TL0 = 0xCC;	// �ҡ��� 0xCC
	TR0 = 0x1;	// �������ҹ TIMER0
	
	// ------------- ; SETUP General program
	setNumberDisplay (10,14);
	setNumberDisplay (9,10);
	setNumberDisplay (8,11);
	setNumberDisplay (7,12);
	setNumberDisplay (6,12);
	setNumberDisplay (5,14);
	setNumberDisplay (4,0);
	setNumberDisplay (3,0);
	setNumberDisplay (2,0);
	setNumberDisplay (1,0);
	
	credit = 0;
	allSong = 0;
	countKey = 0;

	songMemory[0] = 0xFF;
	songMemory[1] = 0xFF;
	songMemory[2] = 0xFF;

	currentSong[0] = 0xFF;
	currentSong[1] = 0xFF;
	currentSong[2] = 0xFF;
}


// ��ҹ��� keyboard matrix ���觢����Ũе�ͧ�� 4 �Ե��, 4 �Ե��ҧ���Ѻ
unsigned char readKeyMatrix(unsigned char valueInput)
{
	idata unsigned char xx;
	idata unsigned char yy;
	
	P1 = valueInput;		// �е�ͧŧ���´��� 1111
	tmpPort = P1;		// read port 1
	tmpPort = tmpPort & 0x0F;	
	if (tmpPort != 0x0F)
	{
		// delay ˹�ǧ���� Debounce
		for (xx = 0; xx < 147; xx++) 
			for (yy = 0; yy < 120; yy++);
			
		//P1 = valueInput;
		tmpPort = P1;		// read port 1
		tmpPort = tmpPort & 0x0F;
		if ((tmpPort & 0x0F) != 0x0F) // �ʴ�����ա�á���ԧ
		{
			for (xx = 0; xx < 147; xx++) 
				for (yy = 0; yy < 120; yy++);
			tmpPort = P1;
			while ((P1 & 0x0F) != 0x0F) {}	// �ѧ����������
			// delay ˹�ǧ���� Debounce
						tmpPort = tmpPort & 0x0F;
			return tmpPort;
		}		
		else
		{
			return 255;
		}
	}	
	return 255;	
}

// ��������˹� location �դ�� number �����ʴ��� �� location �������� 1 ����
void setNumberDisplay (unsigned char location, unsigned char number)
{
	// ��� mod ��������� ����ͧź 1
	// ��� mod ������������ ���ź 1
	tmpModNumber2 = location % 2;
	tmpDivNumber2 = location / 2;
	
	if (tmpModNumber2 == 0)
	{
		tmpDivNumber2--;		
	}
	
	// �������������ҵ����ҧ ����������ҵ�Ǻ�
	if (tmpModNumber2 > 0)
	{
		displayNumber[tmpDivNumber2] = (displayNumber [tmpDivNumber2] & 0xF0) + (number & 0x0F);
	}
	else
	{
		displayNumber[tmpDivNumber2] = (displayNumber [tmpDivNumber2] & 0x0F) + (number << 4);
	}
}

// timer 0 interrupt ����Ѻ�ʴ��ŵ���Ţ
void timer0 (void) interrupt 1
{
	TR0 = 0;// ��ش�����ҹ Timer 0
	if (++displayLocation > _MAX_LOCATION_NUMBER) displayLocation = 1;

	// ��� mod ��������� ����ͧź 1
	// ��� mod ������������ ���ź 1
	tmpModNumber = displayLocation % 2;
	tmpDivNumber = displayLocation / 2;
	
	if (tmpModNumber == 0)
	{
		tmpDivNumber--;		
	}
	
	// �������������ҵ����ҧ ����������ҵ�Ǻ�
	if (tmpModNumber > 0)
	{
		P0 = numberTable[displayNumber [tmpDivNumber] & 0x0F];	
	}
	else
	{
		P0 = numberTable[displayNumber [tmpDivNumber] >> 4];	
	}

	// ��˹��ʴ��Ť�һѨ�غѹ � ��ѡ��ҧ�
	P2 &= 0xF0;
	
	// P2 += (displayLocation-1); // 74LS145 start at 0  
	P2 +=  10 - displayLocation; // *****��䢡���ʴ��Ũҡ����仢�ҷ��ç���
		
	// ��ǹ˹�ǧ�����ʴ�����ѧ��á���������͡�ŧ����
	// ����ա���͡��ź ��ѧ�ҡ���ŧ���� ���ǡ������ҡ������������
	if ((flagKey & 0x04) >> 2) // 0000 0100
	{
		if (delayTimeDisplay == 0)
		{
			flagKey &= 0xFB;		// 1111 1011
			// ��ҡ��ŧ�������� �����������ŧ��ҧ�͡��ź ����ʴ����ŧ�Ѩ�غѹ
			if ((((flagKey & 0x0C) >> 2) == 0) && (allSong < _MAX_SONG))			// 0000 1100
			{
	   			// update current song
				setNumberDisplay (5,(currentSong[0] & 0x0F));
				setNumberDisplay (6,(currentSong[0] >> 4));
				setNumberDisplay (7,(currentSong[1] & 0x0F));
				setNumberDisplay (8,(currentSong[1] >> 4));
				setNumberDisplay (9,(currentSong[2] & 0x0F));
				setNumberDisplay (10,(currentSong[2] >> 4));
			}
			
		}
		else
		{
			if ((--delayTimeDisplay2) == 0)
			{
				delayTimeDisplay2 = 250;
				--delayTimeDisplay;
			}
		}
	}

	// ��Ҥ����������������ѧ����ŧ���� ����ѧ���ŧ��ҧ�˹��¤����� ��Ӥ�ҵ�ҧ� ���蹹�鹡稺
	if ((!((flagKey & 0x02) >> 1)) && (allSong > 0))	// 0000 0010
	{
		// ��Ҥ���ŧ�������� ������ҡ 0 件֧ 2
		sendSerial ('A');
		sendSerial ((songMemory[0] >> 4)+48);
		sendSerial ((songMemory[0] & 0x0F)+48);
		sendSerial ((songMemory[1] >> 4)+48);
		sendSerial ((songMemory[1] & 0x0F)+48);
		sendSerial ((songMemory[2] >> 4)+48);
		sendSerial ((songMemory[2] & 0x0F)+48);
		
		// set bit �����Ҥ�����ѧ����ŧ����
		flagKey |= 0x02;			// 0000 0010
		
		// ��Ҥ���ŧ������������������ currentSong
		currentSong[2] = songMemory[0];
		currentSong[1] = songMemory[1];
		currentSong[0] = songMemory[2];
		
		// Ŵ�ŧ�˹��¤�����
		--allSong;

		// ��ҡ��ŧ����������� refresh ����Ţ
		if ((countKey < 1) && (allSong < _MAX_SONG))		// 0000 1000
		{
			setNumberDisplay (5,(currentSong[0] & 0x0F));
			setNumberDisplay (6,(currentSong[0] >> 4));
			setNumberDisplay (7,(currentSong[1] & 0x0F));
			setNumberDisplay (8,(currentSong[1] >> 4));
			setNumberDisplay (9,(currentSong[2] & 0x0F));
			setNumberDisplay (10,(currentSong[2] >> 4));
		}

		// ����͹�ŧ�����
		for (tmpDivNumber = 0; tmpDivNumber < allSong; tmpDivNumber++)
		{
			// tmpModNumber, tmpDivNumber �����������
			tmpModNumber = tmpDivNumber * 3;
			songMemory[tmpModNumber] = songMemory[tmpModNumber +3];
			songMemory[tmpModNumber +1] = songMemory[tmpModNumber +4];
			songMemory[tmpModNumber +2] = songMemory[tmpModNumber +5];
		}

		// updateCredit & allSong
		setNumberDisplay (4,allSong / 10);	
		setNumberDisplay (3,allSong % 10);
		setNumberDisplay (2,credit / 10);
		setNumberDisplay (1,credit % 10);
	}

	// ��ǹ��͹��	
	TH0 = 0xF8; 		// ��˹������� TIMER0 (�ҡ��� 0xF8)
	TL0 = 0xCC;		// �ҡ��� 0xCC
	TF0 = 0;			// clear FLAG
	TR0 = 1;			// ����������ҹ Timer 0
}

// external interrupt ����Ѻ�ӹǹ�����ʹ����­
void coin (void) interrupt 0
{
	idata unsigned char xx2;
	idata unsigned char yy2;
	EX0 = 0;
	// delayCoin () 
	for (xx2 = 0; xx2 < 147; xx2 ++) 
		for (yy2 = 0; yy2 < 80; yy2 ++); // 30 ms

	if ((!_INSERT_COIN) && (credit < 99))
	{
		if (_P2_4)   //	��Ң� P2.4 ��� set jumper ��� + 1
		{
			credit++;
		}
		else
		{
			credit += 2;	// �ǡ 2 credit
		}
		// updateCredit & allSong
		setNumberDisplay (4,allSong / 10);	
		setNumberDisplay (3,allSong % 10);
		setNumberDisplay (2,credit / 10);
		setNumberDisplay (1,credit % 10);
	}    	
	EX0 = 1;
}

// serial interrupt ������áѺ computer ���� RS232
void serial (void) interrupt 4
{
	idata unsigned char count;
	idata unsigned char location;
	
	ES = 0;
	if (RI == 1)
	{
		if (SBUF == 'R') // request
		{
			// clear flag computer (�Ե��� 1) ���ͺ͡��Ҥ����ҧ����
			flagKey &= 0xFD; // 1111 1101 �ӨѴ�Ե 1

			// ����ѧ���ŧ�����������ͧź˹�Ҩ� ����������ǡ� refresh ����Ţ (Bit 3)
			if (countKey < 1)		// 0000 1000
			{
				setNumberDisplay (5,15);
				setNumberDisplay (6,15);
				setNumberDisplay (7,15);
				setNumberDisplay (8,15);
				setNumberDisplay (9,15);
				setNumberDisplay (10,15);
			}
			
			// ����ŧ�˹��¤��������������
			if (allSong == 0)			// 0000 0010
			{
				// ���ź�ŧ������������ 0 ����纤����ҧ����ŧ� current song ����
				currentSong[2] = 0xFF;
				currentSong[1] = 0xFF;
				currentSong[0] = 0xFF;
			}
   			
			// updateCredit & allSong
			setNumberDisplay (4,allSong / 10);	
			setNumberDisplay (3,allSong % 10);
			setNumberDisplay (2,credit / 10);
			setNumberDisplay (1,credit % 10);
		}
		else if (SBUF == 'P')  // prompt
		{			
			// setup flag 0 -> 1
			flagKey |= 0x01;		// 0000 0001
			
			// setup number
			setNumberDisplay (10,15);
			setNumberDisplay (9,15);
			setNumberDisplay (8,15);
			setNumberDisplay (7,15);
			setNumberDisplay (6,15);
			setNumberDisplay (5,15);

			// open port for connect sound and picture			
		}
		else if (SBUF == 'A')  // add credit
		{			
			credit++;			
		}
		RI = 0;
	}
	ES = 1;
}
