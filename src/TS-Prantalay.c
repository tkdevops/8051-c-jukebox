// BUG 
// - ���Ѵ�ŧ�������� hang

// Prantalay Board
/*
	project : TS - Karaoke Board 1.0 (Hot Track version)
	start	: 12 December 2003 16:25
	target	: AT89C52 clock 11.0592Mhz
	by		: BenNueng 
*/


/*	part of include LIB file */
#include <reg52.h>
#include <intrins.h>
#include <stdio.h> 

//#include <stdio.h>
//#include <at89s8252.h>

/* part of define */
#define __DEBUG__MODE__
//#define __RELEASE__MODE__
#define _MAX_LOCATION_NUMBER				10			// �ӹǹ���˹� 7segment ������
#define _MAX_ARRAY_NUMBER					5			// ��Ҵ���������红����� _MAX_LOCATION_NUMBER (1 byte = 2 �ѡ��)
#define _ARRAY_BUFFER						60			// ���������ŧ
#define _MAX_SONG							20			// �ŧ�٧�ش�������_ARRAY_BUFFER / 3
#define _INSERT_COIN_LEG					P3^2		// ����ʹ����­ 1
#define _INSERT_COIN_LEG2					P3^3		// ����ʹ����­ 2
#define _P2_4_LEG							P2^4		// DipSwitch 1
#define _P2_5_LEG							P2^5		// DipSwitch 2
#define _P2_6_LEG							P2^6		// DipSwitch 3
#define _P2_7_LEG							P2^7		// DipSwitch 4
#define _P3_SOUND1_LEG						P3^4		// Sound1
#define _P3_SOUND2_LEG						P3^5		// Sound2
#define _P3_I2C_DATA_LEG					P3^6		// I2C data leg  WR 16 (�� 2)
#define _P3_I2C_CLOCK_LEG					P3^7		// I2C clock leg  RD 17 (���á)

#define __100ms__							92100
#define __30ms__							27649
#define __25ms__							23041
#define __20ms__							18420
#define __10ms__							9210
#define __1ms__								921

#define true								1
#define false								0
#define byte								unsigned char
#define word								unsigned int


/* part of setup function prototype */
void setup (void);										// ����� xx,yy
void sendSerial (byte ch);								// ����� xx,yy
void keyControl (byte value);							// ����� xx,yy
void setNumberDisplay (byte location, byte number);		// ����� xx,yy
byte readKeyMatrix (byte valueInput);					// �� xx,yy
void delayCycle(word cycle);
void soundNext();
void requestNextSong();

/* part of define byte global variable in ram from function */
// void timer0 (void) interrupt 1
	/* idata */ byte tmpDivNumber;
	/* idata */ byte tmpModNumber;
	/* idata */ byte displayLocation;

// void keyControl (byte value)
	/* idata */ byte countKey;

// byte readKeyMatrix(byte valueInput)
	/* idata */ byte tmpPort;

// void setNumberDisplay (byte location, byte number)
	/* idata */ byte tmpModNumber2;
	/* idata */ byte tmpDivNumber2;
	
/* part of define byte global variable */
data byte songMemory[_ARRAY_BUFFER];						// save song 3 byte per 1 song
data byte currentSong[3];									// �ѹ�֡������������Ѻ�ʴ���

/* idata */ byte displayNumber[_MAX_ARRAY_NUMBER];			// 1 byte = 2 char
/* idata */ byte credit;   									// �纨ӹǹ credit
/* idata */ byte allSong;									// ��������ŧ����ͷ���������ŧ
/* idata */ byte tmpReadkey; 								// ��ҹ�ҡ����
/* idata */ byte delayTimeDisplay;							// ˹�ǧ���ҵ͹���ŧ����
/* idata */ byte delayTimeDisplay2;							// ˹�ǧ���ҵ͹���ŧ����
/* idata */ byte flagKey;									// Flag ��������Ѻ�ҹ��ҧ�

//;		0 PROMPT : ����� 1 �ʴ�������Ѻ��õͺ�Ѻ����
//;		1 COM PLAY  : ����� 1 �ʴ���Ҥ�����������ѧ����ŧ����
//;		2 TIME DISPLAY : ����� 1 �ʴ���ҡ��ѧ���Ţ��衴���������͡�����ź
//;		3 PUSH CONTINUE : ����� 1 �ʴ�����ѧ���ŧ�������
//;		4 SPK0 : �Ե���;  00 = nosound; 01 = left; 11 = both; 10; right
//;		5 SPK1 : �Ե�٧;

/* part of define bit global variable in ram */
sbit _INSERT_COIN	= _INSERT_COIN_LEG;						// ����ʹ����­��� 1
sbit _INSERT_COIN2	= _INSERT_COIN_LEG2;					// ����ʹ����­��� 2
sbit _P2_4			= _P2_4_LEG;							// dipswitch 1
sbit _P2_5			= _P2_5_LEG;							// dipswitch 2
sbit _P2_6			= _P2_6_LEG;							// dipswitch 3
sbit _P2_7			= _P2_7_LEG;							// dipswitch 4
sbit _P3_Sound1		= _P3_SOUND1_LEG;
sbit _P3_Sound2		= _P3_SOUND2_LEG;

//; I2C Defint port
sbit SDA = _P3_I2C_DATA_LEG;
sbit SCL = _P3_I2C_CLOCK_LEG;

//; I2C Define Register
unsigned char dat_buff[8];
bit check_wr = 0;


/* part of define global variable in rom*/
code byte numberTable[] = 
{
	0x3F	,0x06	,0x5B	,0x4F	,0x66	,0x6D	,0x7D	,0x07	,
//	0		1		2       3       4		5		6       7
	0x7F	,0x67	,0x71	,0x3E	,0x38	,0xFF	,0x40	,0x00	,
//	8    	9		F		U		L		8.		-     
	0x76	,0x79	,0x50	,0x5C	,0x39
//	H       E		r		o       C
};


void main (void)
{
	setup();
	while (true)
	{
		#ifdef __RELEASE_MODE__
			while (!(flagKey & 0x01)) {}			// ����ҡ����ѧ��������������Ѻ���
		#endif
		
		// # 9 6 3
		tmpReadkey = readKeyMatrix (0xEF); 		// 1110 1111
		if (tmpReadkey == 0x07)
		{
			// '3'
			//sendSerial('3');
			keyControl (3);
		}
		else if (tmpReadkey == 0x0B)
		{
			// '6'
			//sendSerial('6');
			keyControl (6);
		}
		else if (tmpReadkey == 0x0D)
		{			
			// '9'
			//sendSerial('9');
			keyControl (9);
		}
		else if (tmpReadkey == 0x0E)
		{
			// '#'
			soundNext();
			// Sound Stereo/Mono L/Mono R
		}

		// 0 8 5 2
		tmpReadkey = readKeyMatrix (0xDF);		// 1101 1111
		if (tmpReadkey == 0x07)
		{
			// '2'
			//sendSerial('2');
			keyControl (2);
		}
		else if (tmpReadkey == 0x0B)
		{
			// '5'
			//sendSerial('5');
			keyControl (5);
		}
		else if (tmpReadkey == 0x0D)
		{
			// '8'
			//sendSerial('8');
			keyControl (8);
		}
		else if (tmpReadkey == 0x0E)
		{
			// '0'
			//sendSerial('0');
			keyControl (0);
		}
		
		// * 7 4 1
		tmpReadkey = readKeyMatrix (0xBF);		// 1011 1111
		if (tmpReadkey == 0x07)
		{
			// '1'
			//sendSerial('1');
			keyControl (1);
		}
		else if (tmpReadkey == 0x0B)
		{
			// '4'
			//sendSerial('4');
			keyControl (4);
		}
		else if (tmpReadkey == 0x0D)
		{			
			// '7'
			//sendSerial('7');
			keyControl (7);
		}
		else if (tmpReadkey == 0x0E)
		{
			// '*'
			//sendSerial('*');
			keyControl (15);			
		}

		// D C B A
		tmpReadkey = readKeyMatrix (0x7F);		// 0111 1111
		if (tmpReadkey == 0x07)
		{
			// 'D'
			//sendSerial('D');
		}
		else if (tmpReadkey == 0x0B)
		{
			// 'C'
			//sendSerial('C');
		}
		else if (tmpReadkey == 0x0D)
		{
			// 'B'
			// �Ѵ�ŧ
			//sendSerial('B');
			requestNextSong();			
		}
		else if (tmpReadkey == 0x0E)
		{
			// 'A'
			//sendSerial('A');

			// ���� credit
			credit++;
			// updateCredit & allSong
			setNumberDisplay (4,allSong / 10);	
			setNumberDisplay (3,allSong % 10);
			setNumberDisplay (2,credit / 10);
			setNumberDisplay (1,credit % 10);			
		}
	}
}

// �Ѻ��ҡ�á������ҨѴ�����ФǺ�������ʴ���
void keyControl (byte value)
{

	// read port
	bit bTmp = 0;
	P2 |= 0xF0; // set port 2 is 1111 0000
	bTmp = (P2 & 0x10) >> 4;      // 0001 0000 read bit 4 of port 2
	
	// 
	if (credit <= 0 && bTmp == 0) // bTmp = 1 is insert coin free
	{
		// ���蹹�鹡����ͧ�� credit > 0
		 return;
	}

	if (allSong >= _MAX_SONG) return;		// ����ŧ���

	// �觢�����仺͡����������
	//if (value < 15)
	//{
	//	sendSerial(value+48);
	//}

	// ��Ҥ���� 15 ��͡�����������
	if (value == 15)
	{
		countKey = 0;
		setNumberDisplay (5,(currentSong[0] & 0x0F));
		setNumberDisplay (6,(currentSong[0] >> 4));
		setNumberDisplay (7,(currentSong[1] & 0x0F));
		setNumberDisplay (8,(currentSong[1] >> 4));
		setNumberDisplay (9,(currentSong[2] & 0x0F));
		setNumberDisplay (10,(currentSong[2] >> 4));
	}
	else // if (credit > 0)
	{
		countKey++;
		if (countKey == 1)
		{
			// ¡��ԡ�������������ź�Ţ (clr bit 2) ��� set ʶҹ�������������ҧ��á����� (set bit 3)
			flagKey &= 0xFB;	// 1111 1011
			flagKey |= 0x08;	// 0000 1000
			setNumberDisplay (10,value);
			setNumberDisplay (9,15);
			setNumberDisplay (8,15);
			setNumberDisplay (7,15);
			setNumberDisplay (6,15);
			setNumberDisplay (5,15);
		}
		else if (countKey == 2)
		{
			setNumberDisplay (9,value);
		}
		else if (countKey == 3)
		{
			setNumberDisplay (8,value);
		}
		else if (countKey == 4)
		{
			setNumberDisplay (7,value);
		}
		else if (countKey == 5)
		{
			setNumberDisplay (6,value);
		}
		else if (countKey == 6)
		{
			setNumberDisplay (5,value);
			countKey = 0;					// ������Ѻ�������
			delayTimeDisplay = 4;			// ��˹����˹�ǧ���� 4 �ͺ �ͺ�� 500 MS
			flagKey	&= 0xF7;				// �͡��ҡ������������� 1111 0111
			flagKey |= 0x04;				// �͡������Ţ�͡�����ź 0000 0100
			
			// ����ŧ���� songMemory
			songMemory[allSong*3] = displayNumber[4];
			songMemory[allSong*3+1] = displayNumber[3];
			songMemory[allSong*3+2] = displayNumber[2];
			
			// �����ŧ���Ŵ credit
			if (++allSong == _MAX_SONG)
			{
				// �ʴ������ Full
				setNumberDisplay (10,14);
				setNumberDisplay (9,10);
				setNumberDisplay (8,11);
				setNumberDisplay (7,12);
				setNumberDisplay (6,12);
				setNumberDisplay (5,14);
			}
			
			// Ŵ credit
			// read port
			bTmp = 0;
			P2 |= 0xF0; // set port 2 is 1111 0000
			bTmp = (P2 & 0x10) >> 4;      // 0001 0000 read bit 4 of port 2
			if (bTmp == 0)
			{
				credit--;
			}
			
			// updateCredit & allSong
			setNumberDisplay (4,allSong / 10);	
			setNumberDisplay (3,allSong % 10);
			setNumberDisplay (2,credit / 10);
			setNumberDisplay (1,credit % 10);
		}
	}
}

void sendSerial (byte ch)
{
	//ES = 0;
	//if (!TI)
	if (RI==0)
	{
		SBUF = ch;
		while (TI != 1) {}
		TI = 0;
	}
	//ES = 1;
}

void setup (void)
{
	// ------------- ; port
	P0 = 0xFF;
	P1 = 0xFF;
	P2 = 0xFF;
	P3 = 0xFF;
	
	// ------------- ; interrupt
	EA = 0x1;			// �Դ����� INTERRUPT
	ES = 0x1;			// �Դ INT SERIAL
	PS = 0x1;			// IP PS = 1

	// ------------- ; SETUP INTERRUPT EX1
	IT1 = 0x1;			// TCON REGISTER �ӧҹ���ͺŧ
	EX1 = 0x1;			// ͹حҵ�����ա���� EX1
	PX1 = 0x1;			// ����Ҥ����Ӥѭ�٧

	// ------------- ; SETUP INTERRUPT EX0
	IT0 = 0x0;			// TCON REGISTER �ӧҹ���ͺŧ
	EX0 = 0x1;			// ͹حҵ�����ա���� EX0
	PX0 = 0x1;			// ����Ҥ����Ӥѭ�٧

	// ------------- ; SETUP BUADRATE TIMER1
	// PCON = PCON | 0x80; // �Ե SMOD = 1 ����� PCON ���������Ѵ��ѧ�ҹ
	SCON = 0x50;		// 01 0 1 0000 �Ǻ��� SERIAL
	TMOD = 0x21; 		// TIMER1 MODE 2, TIMER0 MODE 1
	TH1 = 0xFD; 		// �͵�õ 9600 (19200 ���ͧ�絷�� SMOD = 1)
	TR1 = 0x1;			// ������͵�õ

	// ------------- ; SETUP TIMER0 ����ҳ 2ms
	ET0 = 0x1;			// �Դ INT TIMER0
	TH0 = 0xF8; 		// ��˹������� TIMER0 (�ҡ��� 0xF8)
	TL0 = 0xCC;			// �ҡ��� 0xCC
	TR0 = 0x1;			// �������ҹ TIMER0
	
	// ------------- ; SETUP General program
	credit = 0;
	allSong = 0;
	countKey = 0;

	// ����Ţ����觨ҡ����仢�� (��ѡ�á����ҧ����)
	setNumberDisplay (10,14);
	setNumberDisplay (9,10);
	setNumberDisplay (8,11);
	setNumberDisplay (7,12);
	setNumberDisplay (6,12);
	setNumberDisplay (5,14);
	setNumberDisplay (4,0);
	setNumberDisplay (3,0);
	setNumberDisplay (2,0);
	setNumberDisplay (1,0);
	
	songMemory[0] = 0xFF;
	songMemory[1] = 0xFF;
	songMemory[2] = 0xFF;

	currentSong[0] = 0xFF;
	currentSong[1] = 0xFF;
	currentSong[2] = 0xFF;
}


// ��ҹ��� keyboard matrix ���觢����Ũе�ͧ�� 4 �Ե��, 4 �Ե��ҧ���Ѻ
byte readKeyMatrix(byte valueInput)
{
	/* idata */ //byte xx;
	/* idata */ //byte yy;
	
	P1 = valueInput;		// �е�ͧŧ���´��� 1111
	tmpPort = P1;			// read port 1
	tmpPort = tmpPort & 0x0F;	
	if (tmpPort != 0x0F)
	{
		// delay ˹�ǧ���� Debounce
		// for (xx = 0; xx < 147; xx++) 
		//	for (yy = 0; yy < 120; yy++);
		delayCycle(__25ms__);
			
		//P1 = valueInput;
		tmpPort = P1;		// read port 1
		tmpPort = tmpPort & 0x0F;
		if ((tmpPort & 0x0F) != 0x0F) // �ʴ�����ա�á���ԧ
		{
			// debounce
			// for (xx = 0; xx < 147; xx++) 
			//	for (yy = 0; yy < 120; yy++);
			delayCycle(__25ms__);

			tmpPort = P1 & 0x0F;
			while ((P1 & 0x0F) != 0x0F) {}	// �ѧ����������
			
			// delay ˹�ǧ���� Debounce
			// delayCycle(__25ms__);
			return tmpPort;
		}		
		else
		{
			return 255;
		}
	}	
	return 255;	
}

// ��������˹� location �դ�� number �����ʴ��� �� location �������� 1 ����
void setNumberDisplay (byte location, byte number)
{
	// ��� mod ��������� ����ͧź 1
	// ��� mod ������������ ���ź 1
	tmpModNumber2 = location % 2;
	tmpDivNumber2 = location / 2;
	
	if (tmpModNumber2 == 0)
	{
		tmpDivNumber2--;		
	}
	
	// �������������ҵ����ҧ ����������ҵ�Ǻ�
	if (tmpModNumber2 > 0)
	{
		displayNumber[tmpDivNumber2] = (displayNumber [tmpDivNumber2] & 0xF0) + (number & 0x0F);
	}
	else
	{
		displayNumber[tmpDivNumber2] = (displayNumber [tmpDivNumber2] & 0x0F) + (number << 4);
	}
}

// timer 0 interrupt ����Ѻ�ʴ��ŵ���Ţ
void timer0 (void) interrupt 1
{
	TR0 = 0;// ��ش�����ҹ Timer 0
	if (++displayLocation > _MAX_LOCATION_NUMBER)
	{
		displayLocation = 1;
	}

	// �¡��Ҥ�ҵ���Ţ��Ш���ѡ����� port 0
	// ��� mod ��������� ����ͧź 1
	// ��� mod ������������ ���ź 1
	tmpModNumber = displayLocation % 2;
	tmpDivNumber = displayLocation / 2;
	
	if (tmpModNumber == 0)
	{
		tmpDivNumber--;		
	}
	
	// �������������ҵ����ҧ ����������ҵ�Ǻ�
	if (tmpModNumber > 0)
	{
		P0 = numberTable[displayNumber [tmpDivNumber] & 0x0F];	
	}
	else
	{
		P0 = numberTable[displayNumber [tmpDivNumber] >> 4];	
	}

	// ��˹��ʴ��Ť�һѨ�غѹ � ��ѡ��ҧ�
	P2 = 0xF0;
	
	P2 += (displayLocation-1); // 74LS145 start at 0  
	//P2 += 10 - displayLocation; // *****��䢡���ʴ��Ũҡ����仢�ҷ��ç���
		
	// ��ǹ˹�ǧ�����ʴ�����ѧ��á��������͡�ŧ����
	// ����ա���͡��ź ��ѧ�ҡ���ŧ���� ���ǡ������ҡ������������
	if ((flagKey & 0x04) >> 2) // 0000 0100
	{
		if (delayTimeDisplay == 0)
		{
			flagKey &= 0xFB;		// 1111 1011
			// ��ҡ��ŧ�������� �����������ŧ��ҧ�͡��ź ����ʴ����ŧ�Ѩ�غѹ
			if ((((flagKey & 0x0C) >> 2) == 0) && (allSong < _MAX_SONG))			// 0000 1100
			{
	   			// update current song
				setNumberDisplay (5,(currentSong[0] & 0x0F));
				setNumberDisplay (6,(currentSong[0] >> 4));
				setNumberDisplay (7,(currentSong[1] & 0x0F));
				setNumberDisplay (8,(currentSong[1] >> 4));
				setNumberDisplay (9,(currentSong[2] & 0x0F));
				setNumberDisplay (10,(currentSong[2] >> 4));
			}
			
		}
		else
		{
			if ((--delayTimeDisplay2) == 0)
			{
				delayTimeDisplay2 = 250;
				--delayTimeDisplay;
			}
		}
	}

	// ��Ҥ����������������ѧ����ŧ���� ����ѧ���ŧ��ҧ�˹��¤����� ��Ӥ�ҵ�ҧ� ���蹹�鹡稺
	if ((!((flagKey & 0x02) >> 1)) && (allSong > 0))	// 0000 0010
	{
		// ��Ҥ���ŧ�������� ������ҡ 0 件֧ 2
		sendSerial ('Q');
		//delayCycle(__10ms__);
		sendSerial ('\n');
		sendSerial ((songMemory[0] >> 4)+48);
		sendSerial ((songMemory[0] & 0x0F)+48);
		sendSerial ((songMemory[1] >> 4)+48);
		sendSerial ((songMemory[1] & 0x0F)+48);
		sendSerial ((songMemory[2] >> 4)+48);
		sendSerial ((songMemory[2] & 0x0F)+48);
		sendSerial ('\n');
		
		// set bit �����Ҥ�����ѧ����ŧ����
		flagKey |= 0x02;			// 0000 0010
		
		// ��Ҥ���ŧ������������������ currentSong
		currentSong[2] = songMemory[0];
		currentSong[1] = songMemory[1];
		currentSong[0] = songMemory[2];
		
		// Ŵ�ŧ�˹��¤�����
		--allSong;

		// ��ҡ��ŧ����������� refresh ����Ţ
		if ((countKey < 1) && (allSong < _MAX_SONG))		// 0000 1000
		{
			setNumberDisplay (5,(currentSong[0] & 0x0F));
			setNumberDisplay (6,(currentSong[0] >> 4));
			setNumberDisplay (7,(currentSong[1] & 0x0F));
			setNumberDisplay (8,(currentSong[1] >> 4));
			setNumberDisplay (9,(currentSong[2] & 0x0F));
			setNumberDisplay (10,(currentSong[2] >> 4));
		}

		// ����͹�ŧ�����
		for (tmpDivNumber = 0; tmpDivNumber < allSong; tmpDivNumber++)
		{
			// tmpModNumber, tmpDivNumber �����������
			tmpModNumber = tmpDivNumber * 3;
			songMemory[tmpModNumber] = songMemory[tmpModNumber +3];
			songMemory[tmpModNumber +1] = songMemory[tmpModNumber +4];
			songMemory[tmpModNumber +2] = songMemory[tmpModNumber +5];
		}

		// updateCredit & allSong
		setNumberDisplay (4,allSong / 10);	
		setNumberDisplay (3,allSong % 10);
		setNumberDisplay (2,credit / 10);
		setNumberDisplay (1,credit % 10);
	}
	

	// ��ǹ��͹��	
	TH0 = 0xF8; 		// ��˹������� TIMER0 (�ҡ��� 0xF8)
	TL0 = 0xCC;			// �ҡ��� 0xCC
	TF0 = 0;			// clear FLAG
	TR0 = 1;			// ����������ҹ Timer 0
}

// external interrupt ����Ѻ�ӹǹ�����ʹ����­
void coin (void) interrupt 0
{
	///* idata */ byte xx2;
	///* idata */ byte yy2;
	bit bTmp = 0;
	
	EX0 = 0;
	
	// delayCoin () ; debounce
	//for (xx2 = 0; xx2 < 147; xx2 ++) 
	//	for (yy2 = 0; yy2 < 80; yy2 ++); // 30 ms

	// 8 march 2547
	delayCycle(__20ms__);
	if ((!_INSERT_COIN) && (credit < 99))
	{
		// read port		
		P2 |= 0xF0; // set port 2 is 1111 0000
		bTmp = (P2 & 0x20) >> 5;      // 0001 0000 read bit 4 of port 2
	
		if (bTmp)   //	��Ң� P2.5 ��� set jumper ��� + 1
		{
			credit++;
		}
		else
		{
			credit += 2;	// �ǡ 2 credit
		}
		// updateCredit & allSong
		setNumberDisplay (4,allSong / 10);	
		setNumberDisplay (3,allSong % 10);
		setNumberDisplay (2,credit / 10);
		setNumberDisplay (1,credit % 10);

		delayCycle(__100ms__);
	}    	
	EX0 = 1;
}

// external interrupt ����Ѻ�ӹǹ�����ʹ����­ 2
void coin2 (void) interrupt 2
{
	///* idata */ byte xx2;
	///* idata */ byte yy2;
	bit bTmp = 0;
	
	EX1 = 0;
	
	// delayCoin () ; debounce
	//for (xx2 = 0; xx2 < 147; xx2 ++) 
	//	for (yy2 = 0; yy2 < 80; yy2 ++); // 30 ms
	
	// 8 march 2547
	delayCycle(__20ms__);
	if ((!_INSERT_COIN2) && (credit < 99))
	{
		// read port		
		P2 |= 0xF0; // set port 2 is 1111 0000
		bTmp = (P2 & 0x20) >> 5;      // 0001 0000 read bit 4 of port 2
		
		if (bTmp)   //	��Ң� P2.5 ��� set jumper ��� + 1
		{
			credit += 2;
		}
		else
		{
			credit += 4;	// �ǡ 4 credit
		}
		// updateCredit & allSong
		setNumberDisplay (4,allSong / 10);	
		setNumberDisplay (3,allSong % 10);
		setNumberDisplay (2,credit / 10);
		setNumberDisplay (1,credit % 10);

		delayCycle(__100ms__);
	}    	
	EX1 = 1;
}

// serial interrupt ������áѺ computer ���� RS232
void serial (void) interrupt 4
{
	/* idata */ byte count;
	/* idata */ byte location;
	
	ES = 0;
	if (RI == 1)
	{
		if (SBUF == 'R') // request
		{
			requestNextSong();
		}
		else if (SBUF == 'P')  // prompt
		{			
			// setup flag 0 -> 1
			flagKey |= 0x01;		// 0000 0001
			
			// setup number
			setNumberDisplay (10,15);
			setNumberDisplay (9,15);
			setNumberDisplay (8,15);
			setNumberDisplay (7,15);
			setNumberDisplay (6,15);
			setNumberDisplay (5,15);

			// open port for connect sound and picture			
		}
		else if (SBUF == 'L')		// Slogan
		{
			puts("http://www.TalaySoft.com");
		}
		else if (SBUF == 'A')		// add credit
		{			
			credit++;
			// updateCredit & allSong
			setNumberDisplay (4,allSong / 10);	
			setNumberDisplay (3,allSong % 10);
			setNumberDisplay (2,credit / 10);
			setNumberDisplay (1,credit % 10);
		}
		RI = 0;
	}
	ES = 1;
}

// delay by Timer2; 
// ����˹����ҷ���ͧ����� cycle ����ͧ���˹�ǧ
void delayCycle(word cycle)
{	
	TF2 = 0;
	TR2 = 0;
	T2CON = 0x00;

	TH2 = cycle >> 8;	
	TL2 = cycle & 0x00FF;
	TR2 = 1;
	while(!TF2){}
	TF2 = 0;
	TR2 = 0;
}

void soundNext()
{
	//;		4 SPK0 : �Ե���;  00 = nosound; 01 = left; 11 = both; 10; right
	//;		5 SPK1 : �Ե�٧;
	byte tmpFlag = (flagKey & 0x30) >> 4;
	tmpFlag ++;
	if (tmpFlag > 3)
	{
		tmpFlag = 0;
	}
	
	switch(tmpFlag)
	{
		case 0:
			{
				_P3_Sound1 = 0;
				_P3_Sound2 = 0;
				break;
			}
		case 1:
			{
				_P3_Sound1 = 0;
				_P3_Sound2 = 1;
				break;
			}
		case 2:
			{
				_P3_Sound1 = 1;
				_P3_Sound2 = 0;
				break;
			}
		case 3:
			{
				_P3_Sound1 = 1;
				_P3_Sound2 = 1;
				break;
			}
	}

	// save flagKey
	tmpFlag <<= 4;
	flagKey = (flagKey & 0xCF) + tmpFlag;
}


void requestNextSong()
{

	// �觤�Һ͡ com
	sendSerial ('Q');
	//delayCycle(__10ms__);
	sendSerial ('\n');
	//delayCycle(__10ms__);
	sendSerial ('Q');
	//delayCycle(__10ms__);
	sendSerial ('\n');

	// clear flag computer (�Ե��� 1) ���ͺ͡��Ҥ����ҧ����
			flagKey &= 0xFD; // 1111 1101 �ӨѴ�Ե 1

			// ����ѧ���ŧ�����������ͧź˹�Ҩ� ����������ǡ� refresh ����Ţ (Bit 3)
			if (countKey < 1)			// 0000 1000
			{
				setNumberDisplay (5,15);
				setNumberDisplay (6,15);
				setNumberDisplay (7,15);
				setNumberDisplay (8,15);
				setNumberDisplay (9,15);
				setNumberDisplay (10,15);
			}
			
			// ����ŧ�˹��¤��������������
			if (allSong == 0)			// 0000 0010
			{
				// ���ź�ŧ������������ 0 ����纤����ҧ����ŧ� current song ����
				currentSong[2] = 0xFF;
				currentSong[1] = 0xFF;
				currentSong[0] = 0xFF;
			}
   			
			// updateCredit & allSong
			setNumberDisplay (4,allSong / 10);	
			setNumberDisplay (3,allSong % 10);
			setNumberDisplay (2,credit / 10);
			setNumberDisplay (1,credit % 10);

}